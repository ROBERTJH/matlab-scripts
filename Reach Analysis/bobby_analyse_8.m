%-----------------------------------------------------------------------------------------------------
%REACHING ANALYSIS SUB-ROUTINE

%October 29,2013

%R. Hermosillo
%Sensorimotor Neuroscience Laboratory
%Department of Health and Exercise Science
%University of British Columbia Okanagan
%------------------
% Added quartile calculations of position.
% Added pick of primary submovement (by 1st zero cross)
% -- quartiles still based on full movment
% Added selection of primary axis for plot and analysis
% Added ability to accept data with multiple items/markers but only
% analyzes the marker that moves the most and outputs its data
% Added a summary figure for the trial display
% fixed the edit command to the velocity plot
%-----------------------------------------------------------------------------------------------------

%January 11,2013
%R. Hermosillo
%Sensorimotor Neuroscience Laboratory
%Department of Health and Exercise Science
%University of British Columbia Okanagan
%------------------
% Removed quartiles
% Added catch for bad interpolation or if no interpolation is needed
% Program now calulates start and end positions.
% Figures are repoisitioned
%-----------------------------------------------------------------------------------------------------

%July 30,2015
%R. Hermosillo
%Hearing and Speech Sciences
%University of Washington
%------------------
%Subroutine writtent to load position data written
%Routine re-written for one-handed movement
% Figures show the movement trajectory as well as the position of the
% target
% Kinematic Analysis also calculates reach angle, provided that the home
% position it at (0,0)
%-----------------------------------------------------------------------------------------------------

%December 14,2015
%R. Hermosillo
%Hearing and Speech Sciences
%University of Washington
%------------------
%Subroutine writtent to load position data written to show hand
%trajectories
%position of the hand tranjectories are saved and written to a file.



%First, a few hard coded variables
clear all; close all; clc % Let's start fresh!

cancelled = false;
frequency = 240;
[butt_cutoff] = 10;
[butt_order] = 2;
Wn = butt_cutoff/(frequency/2);
[plot_azimuth] = 125;
[plot_elevation] = -13;
i = 1; 
[b,a] = butter(butt_order, Wn,'low');
U = [];
deviationangle_array = []; 
position_array_X = zeros(194,960);
position_array_Y = zeros(194,960);
position_array_long = zeros(2,187200);

%for building an array to graph the changes in deviationangle


%START OF KINEMATIC ANALYSIS OF HAND
%___________________________________________________________________________
disp('***Please select File Directory, then select the file of interest, then the file name***');

%[directory] = uigetdir('E:\Formant and visuomotor rotation\visuomotor\Data\','Please Select the Correct Experiment Folder:');
directory = 'E:\Formant and visuomotor rotation\visuomotor\Data\';
header = {'Trial','Condition','Reaction Time','Movement Time','True Angle','Deviation Angle','Peak Acc'...
    ,'TPA','Peak Vel','TPV','xPV','yPV','Xstart','Ystart','Zstart','Xend','Yend','Zend','1DX','2DX','3DX','4DX'...
    ,'5DX','6DX','7DX','8DX','9DX','10DX','1DY','2DY','3DY','4DY','5DY','6DY','7DY','8DY','9DY','10DY',}; % this line defines header titles; this will become the column title.
uiopen; %open the file of interest
file_name = uigetfile; %what's the name of the file that you selected?
%DATA PREPATION (NECESSARY FOR POLHEMUS LIBERTY DATA)
%___________________________________________________________________________

disp('***BATCH Re-referenceing, this is done once***');
filedata = load(strcat(directory,file_name));
for tr = 1:filedata.setup.trials;

    trial.type  = filedata.type;
    trial.center_radius  = filedata.setup.center_radius;
    trial.target_radius  = filedata.setup.cursor_radius;
    trial.target = filedata.setup.trial_target(tr);
    trial.target_position = filedata.setup.target_position(:,trial.target)*filedata.setup.target_distance;

    trial.target_angle = filedata.setup.target_angle(trial.target);
    trial.rotation_angle = filedata.setup.trial_rotation(tr);
    trial.real_angle = trial.target_angle - trial.rotation_angle;

    amat = [ cos(trial.rotation_angle), -sin(trial.rotation_angle); sin(trial.rotation_angle), cos(trial.rotation_angle) ];
    trial.real_position = amat'*trial.target_position;

    position = liberty_point(filedata.data.liberty{tr},filedata.setup.finger_length);
    position = position - repmat(filedata.data.center_position,1,size(position,2));
    position = position(1:3,:);
    
    %     load b
    %     position = filter(b,1,position')';
    %     position = position(:,1+fix(length(b)/2):end);

    trial.position = position;
    position_data{1,tr} = trial;      
     if tr>1 && tr<=195
     position_array_X(tr,:) = position(1,1:960);
     position_array_Y(tr,:) = position(2,1:960);
     tr_lg = tr*960; %write a long file where the 'xdimension of the array is defined by the number of trials)
     tr_lg_end = tr_lg+959;
     position_array_long(1,(tr_lg:(tr_lg_end))) = position(1,1:960);
     position_array_long(2,(tr_lg:(tr_lg_end))) = position(2,1:960);
     end   
    transpose(position_array_long);
     
end;

plot(position_array_long(1,:),position_array_long(2,:));


temp_subject_name = strsplit(file_name,'_'); %split the file name up so that ".mat" leaves so we can write to excel.
subject_name = temp_subject_name(1); %What is the second element of the array after the name is split?
subject_code = char(subject_name); %convert the cell from the split into a character.
subject_trajectory = strcat(subject_code,'_traj');
xlswrite(subject_code,header,1,'A1'); %excel write...
xlswrite(subject_trajectory,1,1,'A1');
target_position_4 = [8.6603 -5];
target_position_12 = [0 10];
target_position_8 = [-8.6603 -5];

%numframes = cell2mat(Max);
angle12=[];
angle4=[];
angle8=[];

for i=1:195 %go to the next trial
    trial_number = i;
    trial_number = int2str(trial_number);
    numframes = size(position_data{1,i}.position(1,:));
    t = (1:1:numframes(2)); %frames numbers used for plots
    dt = (1:1:(numframes(2)-1));% frame numbers used for velocity plots
    ddt = (1:1:(numframes(2)-2));% frame numbers used for acceleration plots
    target_location_x = position_data{1,i}.target_position(1,1);
    target_location_y = position_data{1,i}.target_position(2,1);
    
    
    %define position vectors
    pos_x = [];pos_y = [];pos1_z = [];
    
    pos_x =(position_data{1,i}.position(1,:));
    pos_y =(position_data{1,i}.position(2,:));
    pos_z =(position_data{1,i}.position(3,:));
    %put back into 3xn format for transform
    data_pos_out = [];
    data_pos_out = [pos_x' pos_y' pos_z'];  % put them into 1 array
    
    %      %% Run interpolation on missing data
    %
    %      %new_matrix=replace(data_pos1_out<-30000000), NaN)
    %      try
    %          bad1 = find(data_pos_out<(-3000000000));
    %          for i = 1:size(bad1)
    %              data_pos_out(bad1(i)) = NaN;
    %              i=i+1;
    %          end
    %          data_pos1_int = naninterp(data_pos_out);
    %
    %    disp('***** WORKING(filter) *****');
    %      catch
    %         disp('Data is not interpolate-able or does not requireinterpolation')
    %      end
    %%
    
    %try %calulating kinematics and displaying graphs
    % low pass filters the data to remove noise
    x1 = []; y1 = []; z1 = [];x2 = []; y2 = []; z2 = [];
    x1 = filtfilt(b,a,data_pos_out(:,1));
    y1 = filtfilt(b,a,data_pos_out(:,2));
    z1 = filtfilt(b,a,data_pos_out(:,3));
    
    % Differentiate with 5-point CFD twice per axis
    [dx1] = filtfilt(b,a,diff(x1).*(frequency));
    [ddx1] = filtfilt(b,a,diff(dx1).*(frequency));
    [dy1] = filtfilt(b,a,diff(y1).*(frequency));
    [ddy1] = filtfilt(b,a,diff(dy1).*(frequency));
    [dz1] = filtfilt(b,a,diff(z1).*(frequency));
    [ddz1] = filtfilt(b,a,diff(dz1).*(frequency));
    
    %Rebuild into a 3-D trajectory and estimate V & A on 's'
    temp1 = (dx1.^2 + dy1.^2 + dz1.^2);
    ds1=temp1.^(0.5);
    dds1=filtfilt(b,a,diff(ds1).*(frequency));
    tmp1 = 1;
    
    %Find movement initiation and end
    j1=1; % j1 is a counter for currently extraced frame from hand1
    j2=1; % j2 is a counter for currently extraced frame from hand2
    %be conservative to find start
    while(ds1(j1)<25 && ds1(j1+2)<25 && ds1(j1+5)<25)
        if (j1>=numframes-21)
            break;
        end
        j1=j1+1;
        tmp1=j1;
    end
    j1=tmp1+5;
    
    %roll back to find more liberal RT
    while(ds1(j1)>1)
        if (j1<=2)
            break;
        end
        j1=j1-1;
    end
    
    rt_frame1 = j1;
    j1=tmp1+10;
    
    %find movement end
    while((j1+30)<(numframes(2)-10))
        if(ds1(j1)<20 && ds1(j1+2)<20 && ds1(j1+4)<20 && ds1(j1)>ds1(j1-1))
            break;
        end
        j1=j1+1;
    end
    end_frame1 = j1;
    
    
    if rt_frame1 > end_frame1
        end_frame1 = rt_frame1 + 2;
        disp('Problem determining movement start')
    end
    
    
    % Find the peaks in each axis (and 's')
    disp('***** WORKING(peak-pick) *****');
    %peak vel1
    [pvx1,ipvx1] = max(abs(dx1(rt_frame1:end_frame1)));
    [pvy1,ipvy1] = max(abs(dy1(rt_frame1:end_frame1)));
    [pvz1,ipvz1] = max(abs(dz1(rt_frame1:end_frame1)));
    [pvs1,ipvs1] = max(abs(ds1(rt_frame1:end_frame1)));
    
    %re-reference times to RT
    ipvx1 = ipvx1+rt_frame1;
    ipvy1 = ipvy1+rt_frame1;
    ipvz1 = ipvz1+rt_frame1;
    ipvs1 = ipvs1+rt_frame1;
    
    %fix signs
    pvx1 = dx1(ipvx1);
    pvy1 = dy1(ipvy1);
    pvz1 = dz1(ipvz1);
    
    %peak accel
    [pax1,ipax1] = max(abs(ddx1(rt_frame1:ipvs1)));
    [pay1,ipay1] = max(abs(ddy1(rt_frame1:ipvs1)));
    [paz1,ipaz1] = max(abs(ddz1(rt_frame1:ipvs1)));
    [pas1,ipas1] = max(abs(dds1(rt_frame1:ipvs1)));
    
    %re-reference to RT
    ipax1 = ipax1+rt_frame1;
    ipay1 = ipay1+rt_frame1;
    ipaz1 = ipaz1+rt_frame1;
    ipas1 = ipas1+rt_frame1;
    
    %fix signs
    pax1 = ddx1(ipax1);
    pay1 = ddy1(ipay1);
    paz1 = ddz1(ipaz1);
    
    %peak decel
    [pdx1,ipdx1] = max(abs(ddx1(ipvs1:end_frame1)));
    [pdy1,ipdy1] = max(abs(ddy1(ipvs1:end_frame1)));
    [pdz1,ipdz1] = max(abs(ddz1(ipvs1:end_frame1)));
    [pds1,ipds1] = max(abs(dds1(ipvs1:end_frame1)));
    
    %re-reference to trial start
    ipdx1 = ipdx1+ipvs1;
    ipdy1 = ipdy1+ipvs1;
    ipdz1 = ipdz1+ipvs1;
    ipds1 = ipds1+ipvs1;
    
    %fix signs
    pdx1 = ddx1(ipdx1);
    pdy1 = ddy1(ipdy1);
    pdz1 = ddz1(ipdz1);
    
    try %to recalculate movement variables (only works with good trials)
        
        %position at x,y,z position @ movement start and end.
        startposx1 = x1(rt_frame1);
        startposy1 = y1(rt_frame1);
        startposz1 = z1(rt_frame1);
        
        endposx1 = x1(end_frame1);
        endposy1 = y1(end_frame1);
        endposz1 = z1(end_frame1);
        
        %time to pv
        tpvx1 = (ipvx1-rt_frame1)*(1000/frequency);
        tpvy1 = (ipvy1-rt_frame1)*(1000/frequency);
        tpvz1 = (ipvz1-rt_frame1)*(1000/frequency);
        tpvs1 = (ipvs1-rt_frame1)*(1000/frequency);
        
        %time to pa
        tpax1 = (ipax1-rt_frame1)*(1000/frequency);
        tpay1 = (ipay1-rt_frame1)*(1000/frequency);
        tpaz1 = (ipaz1-rt_frame1)*(1000/frequency);
        tpas1 = (ipas1-rt_frame1)*(1000/frequency);
        
        %time to pd
        tpdx1 = (ipdx1-rt_frame1)*(1000/frequency);
        tpdy1 = (ipdy1-rt_frame1)*(1000/frequency);
        tpdz1 = (ipdz1-rt_frame1)*(1000/frequency);
        tpds1 = (ipds1-rt_frame1)*(1000/frequency);
        
        % get the location of the peaks (in x,y,and z)
        
        % xyz of pv
        pvxdisp1 = [x1(ipvx1) y1(ipvx1) z1(ipvx1)];
        pvydisp1 = [x1(ipvy1) y1(ipvy1) z1(ipvy1)];
        pvzdisp1 = [x1(ipvz1) y1(ipvz1) z1(ipvz1)];
        pvsdisp1 = [x1(ipvs1) y1(ipvs1) z1(ipvs1)];
        
        % xyz of pa
        paxdisp1 = [x1(ipax1) y1(ipax1) z1(ipax1)];
        paydisp1 = [x1(ipay1) y1(ipay1) z1(ipay1)];
        pazdisp1 = [x1(ipaz1) y1(ipaz1) z1(ipaz1)];
        pasdisp1 = [x1(ipas1) y1(ipas1) z1(ipas1)];
        
        % xyz of pd
        pdxdisp1 = [x1(ipdx1) y1(ipdx1) z1(ipdx1)];
        pdydisp1 = [x1(ipdy1) y1(ipdy1) z1(ipdy1)];
        pdzdisp1 = [x1(ipdz1) y1(ipdz1) z1(ipdz1)];
        pdsdisp1 = [x1(ipds1) y1(ipds1) z1(ipds1)];
        
        true_angle = atan(target_location_y/target_location_x);
        true_angle_deg = true_angle*180/pi;
        reach_angle = atan(pvsdisp1(1)/pvsdisp1(2));
        reach_angle_deg = reach_angle*180/pi;
        tempdeviationangle = reach_angle_deg-true_angle_deg;
        deviation_angle = tempdeviationangle-true_angle_deg;
        if deviation_angle <-90
            
            deviation_angle = deviation_angle+180;
        end
        
    catch
        disp('**Unable to calcuate movement parameters from previous trial**')
    end
    
    %%caluculation of peak vector that connects the start position with the peak velocity.
    MaxTime = size(position_data{1,i}.position,2);
    peakVvec = [];targetVvec = [];
    peakVvec = [position_data{1,i}.position(1,MaxTime);position_data{1,i}.position(2,MaxTime)];
    targetvec= [position_data{1,i}.target_position(1);position_data{1,i}.target_position(2)];
    if (abs(targetvec(1)-8.6603)<0.0001) && ((targetvec(1)*peakVvec(2)-targetvec(2)*peakVvec(1))>0)
    angle4=-acosd(dot(targetvec,peakVvec)/(norm(targetvec)*norm(peakVvec)));
    elseif (abs(targetvec(1)-8.6603)<0.0001) && ((targetvec(1)*peakVvec(2)-targetvec(2)*peakVvec(1))<0)
    angle4=acosd(dot(targetvec,peakVvec)/(norm(targetvec)*norm(peakVvec)));
    elseif (abs(targetvec(1)-0)<0.0001) && ((targetvec(1)*peakVvec(2)-targetvec(2)*peakVvec(1))>0)
    angle12=-acosd(dot(targetvec,peakVvec)/(norm(targetvec)*norm(peakVvec)));
    elseif (abs(targetvec(1)-0)<0.0001) && ((targetvec(1)*peakVvec(2)-targetvec(2)*peakVvec(1))<0)
    angle12=acosd(dot(targetvec,peakVvec)/(norm(targetvec)*norm(peakVvec)));
    elseif (abs(targetvec(1)+8.6603)<0.0001)  && ((targetvec(1)*peakVvec(2)-targetvec(2)*peakVvec(1))>0)
    angle8=-acosd(dot(targetvec,peakVvec)/(norm(targetvec)*norm(peakVvec)));
    else
    angle8=acosd(dot(targetvec,peakVvec)/(norm(targetvec)*norm(peakVvec)));
    end
    
    
    angle_pvel_4=[];
    angle_pvel_8=[];
    angle_pvel_12=[];
    
     if (abs(targetvec(1)-8.6603)<0.0001) && ((targetvec(1)*peakVvec(2)-targetvec(2)*peakVvec(1))>0)
    angle_pvel_4(length(angle_pvel_4)+1)= true_angle*(180/pi)-30;
    elseif (abs(targetvec(1)-8.6603)<0.0001) && ((targetvec(1)*peakVvec(2)-targetvec(2)*peakVvec(1))<0)
    angle_pvel_4(length(angle_pvel_4)+1)= true_angle*(180/pi)-30;
    elseif (abs(targetvec(1)-0)<0.0001) && ((targetvec(1)*peakVvec(2)-targetvec(2)*peakVvec(1))>0)
    angle_pvel_12(length(angle_pvel_12)+1)= true_angle*(180/pi)-90;
    elseif (abs(targetvec(1)-0)<0.0001) && ((targetvec(1)*peakVvec(2)-targetvec(2)*peakVvec(1))<0)
    angle_pvel_12(length(angle_pvel_12)+1)= true_angle*(180/pi)-90;
    elseif (abs(targetvec(1)+8.6603)<0.0001)  && ((targetvec(1)*peakVvec(2)-targetvec(2)*peakVvec(1))>0)
    angle_pvel_8(length(angle_pvel_8)+1)= true_angle*(180/pi)+30;
    else
    angle_pvel_8(length(angle_pvel_8)+1)= true_angle*(180/pi)+30;
    end
    
    %view and fix data if necessary
    [rect1] = [0.00292969 0.532552 0.305703 0.343698];
    %fignam = strcat('trial',int2str(file_number),': Position');
    fignam1 = strcat(trial_number,': Position');
    fig1=figure('Name',fignam1,'NumberTitle','off','Units','normalized','Position',rect1);
    hold on
    plot (t,x1-x1(1),'Color','k');
    plot(t,y1-y1(1), 'Color',[0.4,0,0.7]);
    plot(t,z1-z1(1),'Color',[0,0.1,0.85]);
    title('Hand Position1');
    legend('X-displacement','Y-displacement','Z-displacement');
    grid on;
    v=axis;
    xlabel('Time(samples)');
    ylabel('Displacement(mm)');
    line([rt_frame1,rt_frame1],[v(3),v(4)],'Color','g');
    %line([end_prime,end_prime],[v(3),v(4)],'Color','w');
    line([end_frame1,end_frame1],[v(3),v(4)],'Color','r');
    whitebg([1,1,1]);
    hold off
    
    [rect2] = [0.00292969 0.005 0.6 0.343698];
    %fignam2 = strcat('trial',int2str(input_file),': Velocity');
    fignam2 = strcat(trial_number,': Velocity');
    fig2=figure('Name',fignam2,'NumberTitle','off','Units','normalized','Position',rect2);
    hold on
    plot(dt,dx1,'Color','k');
    plot(dt,dy1,'Color',[0.4,0,0.7]);
    plot(dt,ds1,'Color',[0,0.1,0.85]);
    title('Hand Speed1');
    legend('X-velocity','Y-velocity','S-velocity');
    xlabel('Time(samples)');
    ylabel('Velocity (mm/s)');
    grid on;
    v=axis;
    line([ipvs1,ipvs1],[v(3),v(4)],'Color','m');
    line([rt_frame1,rt_frame1],[v(3),v(4)],'Color','g');
    %line([end_prime,end_prime],[v(3),v(4)],'Color','w');
    line([end_frame1,end_frame1],[v(3),v(4)],'Color','r');
    %whitebg([0.1,0.65,1]);
    hold off
    
%     [rect3] = [0.355,0.0643 0.305703 0.343698];
%     %fignam = strcat('trial',int2str(input_file),': Acceleration');
%     fignam3 = strcat(trial_number,': Acceleration');
%     fig3 = figure('Name',fignam3,'NumberTitle','off','Units','normalized','Position',rect3);
%     plot(ddt,dds1,'Color','k');
%     title('Hand Acceleration');
%     legend('S-accel');
%     xlabel('Time(samples)');
%     ylabel('Acceleration (mm/s2)');
%     grid on;
%     v=axis;
%     line([ipvs1,ipvs1],[v(3),v(4)],'Color','y');
%     line([rt_frame1,rt_frame1],[v(3),v(4)],'Color','g');
%     %line([end_prime,end_prime],[v(3),v(4)],'Color','w');
%     line([end_frame1,end_frame1],[v(3),v(4)],'Color','r');
%     %whitebg([0.1,0.65,1]);
    
    [rect4] = [0.355 0.532552 0.343698 0.343698];
    %fignam = strcat('trial',int2str(input_file),': Acceleration');
    fignam4 = strcat(trial_number,': Trajectory');
    fig4 = figure('Name',fignam4,'NumberTitle','off','Units','normalized','Position',rect4);
    hold on
    plot(position_array_long(1,:),position_array_long(2,:));
    plot(target_position_4(1),target_position_4(2),'r.-','MarkerSize',20);
    plot(target_position_8(1),target_position_8(2),'r.-','MarkerSize',20);
    plot(target_position_12(1),target_position_12(2),'r.-','MarkerSize',20);
    line([0, pvsdisp1(1)],[0,pvsdisp1(2)],'Color','b');
    comet(x1(1:400),y1(1:400));
    plot(x1,y1,'Color','r');
    plot(0,0,'ko:','MarkerSize',10);
    plot(pvsdisp1(1),pvsdisp1(2),'ko:','MarkerSize',5);
    line([0, pvsdisp1(1)],[0,pvsdisp1(2)],'Color','b');
    plot(target_location_x,target_location_y,'r.-','MarkerSize',20);
    line([0, target_location_x],[0,target_location_y],'Color','r');
    plot(peakVvec(1),peakVvec(2),'k*:','MarkerSize',5);
    legend(strcat('Deviation Angle:', num2str(deviation_angle)))
    title('Trajectory');
    grid on;
    xlabel('Displacement along the x-axis (mm)');
    ylabel('Displacement along the y-axis (mm)');
    hold off;
    
    
    %----------------------------------------------------------
    [rect5] = [0.71 0.0643 0.2503 0.4030698];
    %fignam4 = strcat('trial',int2str(input_file),': 3 Dimensional View of Displacement');
    fignam5 = strcat(trial_number,': trial slope');
    fig5=figure('Name',fignam5,'NumberTitle','off','Units','normalized','Position',rect5);
    deviationangle_array(i) = deviation_angle;
    anglesforplotting = deviationangle_array;
    tempi = 1:i;
    line([0,i],[30,30],'Color','b');
    hold on
    try
        plot (tempi,anglesforplotting,'r.-','MarkerSize',20);
    catch
    end
    
    grid on
    %whitebg([0.1,0.65,1]);
    xlabel('Trial number');
    ylabel('angle at Peak velocity (mm)');
    title('3-Dimensional view of Hand Displacement (mm)');
    hold off
    
    
    %     disp('***** ViewPlots *****'); %this next portion is from reach_nalyse_7b. Look there for coding.
    %     fignam6 = strcat('Trial Summaries');
    %     set(0,'CurrentFigure',fig5);
    %     rt1 =(int2str(rt_frame1*(1000/frequency)));
    %     mt1 =(int2str((end_frame1 - rt_frame1)*(1000/frequency)));
    %     pa1 =(int2str(pas1));
    %     tpa1 = (int2str((ipas1-rt_frame1)*(1000/frequency)));
    %     pv1 = (int2str(pvs1));
    %     tpv1 = (int2str((ipvs1-rt_frame1)*(1000/frequency)));
    %     pd1 = (int2str(pds1));
    %     tpd1 = (int2str((ipds1-rt_frame1)*(1000/frequency)));
    %
    %     rt2 =(int2str(rt_frame2*(1000/frequency)));
    %     mt2 =(int2str((end_frame2 - rt_frame2)*(1000/frequency)));
    %     pa2 =(int2str(pas2));
    %     tpa2 = (int2str((ipas2-rt_frame2)*(1000/frequency)));
    %     pv2 = (int2str(pvs2));
    %     tpv2 = (int2str((ipvs2-rt_frame2)*(1000/frequency)));
    %     pd2 = (int2str(pds2));
    %     tpd2 = (int2str((ipds2-rt_frame2)*(1000/frequency)));
    %     [fig6 Uinput Comments] = reach_trial_summary_table(rt,mt,pv,tpv,pa,tpa,pd,tpd,fignam6,pos5,M_s_dist,tmsd);
    
    %------------------------------------------------------------
    %catch Bad_interp1
    %disp('Problem calculating movement: data IRED may not seen. Trial kill suggested')
    %end
    
    %pause(0.25);
    disp('***** Press SPACE BAR for next trial, "K" to kill, or "E" to Edit *****');
    while(1)%Start of Trial Editor
        w = waitforbuttonpress;
        if w == 1
            disp('Key press')
        end
        input = get(gcf,'CurrentCharacter');
        if strcmp(input,'e')==1
            kill = 0;
            figure(fig2)
            disp('***** Indicate the Start and End of the trial (IRED1)using the mouse *****');
            [temp,rt1] = ginput(1);
            rt_frame1=round(temp);
            vel_at_rt1=rt1;
            [temp,end1] = ginput(1);
            end_frame1=round(temp);
            vel_at_end1=end1;
            %input = get(gcf,'CurrentCharacter')
            
            % 			rt_frame1 = round(temp1);
            %           rt_frame2 = round(temp2);
            
            Movement_Start1 = rt_frame1;
            X_Start_Vel1 = dx1(rt_frame1);
            Y_Start_Vel1 = dy1(rt_frame1);
            S_Start_Vel1 = ds1(rt_frame1);
            
            %             [temp1,end_val1] = ginput(1);
            %             end_frame1 = round(temp1);
            %             figure(fig4)
            %             [temp2,end_val2] = ginput(1);
            %             end_frame2 = round(temp2);
            
            if end_frame1 < rt_frame1 + 3
                end_frame1 = rt_frame1 + 5;
            end
            
            Movement_End1 = end_frame1;
            X_End_Vel1 = ds1(end_frame1);
            Y_End_Vel1 = dy1(end_frame1);
            S_End_Vel1 = ds1(end_frame1);
            % Re-Find the peaks in each axis (and 's')
            disp('***** RE-WORKING(peak-pick) *****');
            
            %peak vel
            [pvx1,ipvx1] = max(abs(dx1(rt_frame1:end_frame1)));
            [pvy1,ipvy1] = max(abs(dy1(rt_frame1:end_frame1)));
            [pvz1,ipvz1] = max(abs(dz1(rt_frame1:end_frame1)));
            [pvs1,ipvs1] = max(abs(ds1(rt_frame1:end_frame1)));
            
            %re-reference times to RT
            ipvx1 = ipvx1+rt_frame1;
            ipvy1 = ipvy1+rt_frame1;
            ipvz1 = ipvz1+rt_frame1;
            ipvs1 = ipvs1+rt_frame1;
            
            %fix signs
            pvx1 = dx1(ipvx1);
            pvy1 = dy1(ipvy1);
            pvz1 = dz1(ipvz1);
            
            %peak accel
            [pax1,ipax1] = max(abs(ddx1(rt_frame1:ipvs1)));
            [pay1,ipay1] = max(abs(ddy1(rt_frame1:ipvs1)));
            [paz1,ipaz1] = max(abs(ddz1(rt_frame1:ipvs1)));
            [pas1,ipas1] = max(abs(dds1(rt_frame1:ipvs1)));
            
            %re-reference to RT
            ipax1 = ipax1+rt_frame1;
            ipay1 = ipay1+rt_frame1;
            ipaz1 = ipaz1+rt_frame1;
            ipas1 = ipas1+rt_frame1;
            
            %fix signs
            pax1 = ddx1(ipax1);
            pay1 = ddy1(ipay1);
            paz1 = ddz1(ipaz1);
            
            %peak decel1
            [pdx1,ipdx1] = max(abs(ddx1(ipvs1:end_frame1)));
            [pdy1,ipdy1] = max(abs(ddy1(ipvs1:end_frame1)));
            [pdz1,ipdz1] = max(abs(ddz1(ipvs1:end_frame1)));
            [pds1,ipds1] = max(abs(dds1(ipvs1:end_frame1)));
            
            %re-reference to RT
            ipdx1 = ipdx1+ipvs1;
            ipdy1 = ipdy1+ipvs1;
            ipdz1 = ipdz1+ipvs1;
            ipds1 = ipds1+ipvs1;
            
            %fix signs
            pdx1 = ddx1(ipdx1);
            pdy1 = ddy1(ipdy1);
            pdz1 = ddz1(ipdz1);
            try %to recalculate movement variables (only works with good trials)
                
                %position at x,y,z position @ movement start and end.
                startposx1 = x1(rt_frame1);
                startposy1 = y1(rt_frame1);
                startposz1 = z1(rt_frame1);
                
                endposx1 = x1(end_frame1);
                endposy1 = y1(end_frame1);
                endposz1 = z1(end_frame1);
                
                %time to pv
                tpvx1 = (ipvx1-rt_frame1)*(1000/frequency);
                tpvy1 = (ipvy1-rt_frame1)*(1000/frequency);
                tpvz1 = (ipvz1-rt_frame1)*(1000/frequency);
                tpvs1 = (ipvs1-rt_frame1)*(1000/frequency);
                
                %time to pa
                tpax1 = (ipax1-rt_frame1)*(1000/frequency);
                tpay1 = (ipay1-rt_frame1)*(1000/frequency);
                tpaz1 = (ipaz1-rt_frame1)*(1000/frequency);
                tpas1 = (ipas1-rt_frame1)*(1000/frequency);
                
                %time to pd
                tpdx1 = (ipdx1-rt_frame1)*(1000/frequency);
                tpdy1 = (ipdy1-rt_frame1)*(1000/frequency);
                tpdz1 = (ipdz1-rt_frame1)*(1000/frequency);
                tpds1 = (ipds1-rt_frame1)*(1000/frequency);
                
                % get the location of the peaks (in x,y,and z)
                
                % xyz of pv
                pvxdisp1 = [x1(ipvx1) y1(ipvx1) z1(ipvx1)];
                pvydisp1 = [x1(ipvy1) y1(ipvy1) z1(ipvy1)];
                pvzdisp1 = [x1(ipvz1) y1(ipvz1) z1(ipvz1)];
                pvsdisp1 = [x1(ipvs1) y1(ipvs1) z1(ipvs1)];
                
                % xyz of pa
                paxdisp1 = [x1(ipax1) y1(ipax1) z1(ipax1)];
                paydisp1 = [x1(ipay1) y1(ipay1) z1(ipay1)];
                pazdisp1 = [x1(ipaz1) y1(ipaz1) z1(ipaz1)];
                pasdisp1 = [x1(ipas1) y1(ipas1) z1(ipas1)];
                
                % xyz of pd
                pdxdisp1 = [x1(ipdx1) y1(ipdx1) z1(ipdx1)];
                pdydisp1 = [x1(ipdy1) y1(ipdy1) z1(ipdy1)];
                pdzdisp1 = [x1(ipdz1) y1(ipdz1) z1(ipdz1)];
                pdsdisp1 = [x1(ipds1) y1(ipds1) z1(ipds1)];
                
                true_angle = atan(target_location_y/target_location_x);
                true_angle_deg = true_angle*180/pi;
                reach_angle = atan(pvsdisp1(1)/pvsdisp1(2));
                reach_angle_deg = reach_angle*180/pi;
                tempdeviationangle = reach_angle_deg-true_angle_deg;
                deviation_angle = tempdeviationangle-true_angle_deg;
                
                
     if (abs(targetvec(1)-8.6603)<0.0001) && ((targetvec(1)*peakVvec(2)-targetvec(2)*peakVvec(1))>0)
    angle_pvel_4(length(angle_pvel_4)+1)= true_angle*(180/pi)-30;
    elseif (abs(targetvec(1)-8.6603)<0.0001) && ((targetvec(1)*peakVvec(2)-targetvec(2)*peakVvec(1))<0)
    angle_pvel_4(length(angle_pvel_4)+1)= true_angle*(180/pi)-30;
    elseif (abs(targetvec(1)-0)<0.0001) && ((targetvec(1)*peakVvec(2)-targetvec(2)*peakVvec(1))>0)
    angle_pvel_12(length(angle_pvel_12)+1)= true_angle*(180/pi)-90;
    elseif (abs(targetvec(1)-0)<0.0001) && ((targetvec(1)*peakVvec(2)-targetvec(2)*peakVvec(1))<0)
    angle_pvel_12(length(angle_pvel_12)+1)= true_angle*(180/pi)-90;
    elseif (abs(targetvec(1)+8.6603)<0.0001)  && ((targetvec(1)*peakVvec(2)-targetvec(2)*peakVvec(1))>0)
    angle_pvel_8(length(angle_pvel_8)+1)= true_angle*(180/pi)+30;
    else
    angle_pvel_8(length(angle_pvel_8)+1)= true_angle*(180/pi)+30;
     end
     
       if deviation_angle <-90
            
            deviation_angle = deviation_angle+180;
       end
     
            catch
                disp('**Unable to calcuate movement parameters from previous trial**')
            end
            close all;
            
            %--------------------------------------------------------------------------
            % replot figure with new values
            [rect1] = [0.00292969 0.532552 0.305703 0.343698];
    %fignam = strcat('trial',int2str(file_number),': Position');
    fignam1 = strcat(trial_number,': Position');
    fig1=figure('Name',fignam1,'NumberTitle','off','Units','normalized','Position',rect1);
    hold on
    plot (t,x1-x1(1),'Color','k');
    plot(t,y1-y1(1), 'Color',[0.4,0,0.7]);
    plot(t,z1-z1(1),'Color',[0,0.1,0.85]);
    title('Hand Position1');
    legend('X-displacement','Y-displacement','Z-displacement');
    grid on;
    v=axis;
    xlabel('Time(samples)');
    ylabel('Displacement(mm)');
    line([rt_frame1,rt_frame1],[v(3),v(4)],'Color','g');
    %line([end_prime,end_prime],[v(3),v(4)],'Color','w');
    line([end_frame1,end_frame1],[v(3),v(4)],'Color','r');
    whitebg([1,1,1]);
    hold off
    
    [rect2] = [0.00292969 0.0643 0.305703 0.343698];
    %fignam2 = strcat('trial',int2str(input_file),': Velocity');
    fignam2 = strcat(trial_number,': Velocity');
    fig2=figure('Name',fignam2,'NumberTitle','off','Units','normalized','Position',rect2);
    hold on
    plot(dt,dx1,'Color','k');
    plot(dt,dy1,'Color',[0.4,0,0.7]);
    plot(dt,ds1,'Color',[0,0.1,0.85]);
    title('Hand Speed1');
    legend('X-velocity','Y-velocity','S-velocity');
    xlabel('Time(samples)');
    ylabel('Velocity (mm/s)');
    grid on;
    v=axis;
    line([ipvs1,ipvs1],[v(3),v(4)],'Color','m');
    line([rt_frame1,rt_frame1],[v(3),v(4)],'Color','g');
    %line([end_prime,end_prime],[v(3),v(4)],'Color','w');
    line([end_frame1,end_frame1],[v(3),v(4)],'Color','r');
    %whitebg([0.1,0.65,1]);
    hold off
    
    [rect3] = [0.355,0.0643 0.305703 0.343698];
    %fignam = strcat('trial',int2str(input_file),': Acceleration');
    fignam3 = strcat(trial_number,': Acceleration');
    fig3 = figure('Name',fignam3,'NumberTitle','off','Units','normalized','Position',rect3);
    plot(ddt,dds1,'Color','k');
    title('Hand Acceleration');
    legend('S-accel');
    xlabel('Time(samples)');
    ylabel('Acceleration (mm/s2)');
    grid on;
    v=axis;
    line([ipvs1,ipvs1],[v(3),v(4)],'Color','y');
    line([rt_frame1,rt_frame1],[v(3),v(4)],'Color','g');
    %line([end_prime,end_prime],[v(3),v(4)],'Color','w');
    line([end_frame1,end_frame1],[v(3),v(4)],'Color','r');
    %whitebg([0.1,0.65,1]);
    
    [rect4] = [0.355 0.532552 0.343698 0.343698];
    %fignam = strcat('trial',int2str(input_file),': Acceleration');
    fignam4 = strcat(trial_number,': Trajectory');
    fig4 = figure('Name',fignam4,'NumberTitle','off','Units','normalized','Position',rect4);
    hold on
    plot(position_array_long(1,:),position_array_long(2,:));
    plot(target_position_4(1),target_position_4(2),'r.-','MarkerSize',20);
    plot(target_position_8(1),target_position_8(2),'r.-','MarkerSize',20);
    plot(target_position_12(1),target_position_12(2),'r.-','MarkerSize',20);
    line([0, pvsdisp1(1)],[0,pvsdisp1(2)],'Color','b');
    plot(x1,y1);
    plot(0,0,'ko:','MarkerSize',10);
    plot(pvsdisp1(1),pvsdisp1(2),'ko:','MarkerSize',5);
    line([0, pvsdisp1(1)],[0,pvsdisp1(2)],'Color','b');
    plot(target_location_x,target_location_y,'r.-','MarkerSize',20);
    line([0, target_location_x],[0,target_location_y],'Color','r');
    plot(peakVvec(1),peakVvec(2),'k*:','MarkerSize',5);
    legend(strcat('Deviation Angle:', num2str(deviation_angle)))
    title('Trajectory');
    grid on;
    xlabel('Displacement along the x-axis (mm)');
    ylabel('Displacement along the y-axis (mm)');
    hold off;
    
    [rect5] = [0.71 0.0643 0.2503 0.4030698];
    %fignam4 = strcat('trial',int2str(input_file),': 3 Dimensional View of Displacement');
    fignam5 = strcat(trial_number,': trial slope');
    fig5=figure('Name',fignam5,'NumberTitle','off','Units','normalized','Position',rect5);
    deviationangle_array(i) = deviation_angle;
    anglesforplotting = deviationangle_array;
    tempi = 1:i;
    
    hold on
    try
        plot (tempi,anglesforplotting,'r.-','MarkerSize',20);
    catch
    end
    
    grid on
    %whitebg([0.1,0.65,1]);
    xlabel('Trial number');
    ylabel('angle at Peak velocity (mm)');
    title('3-Dimensional view of Hand Displacement (mm)');
    hold off

    
    pause(3);
            %------------------------------------------------------------
            break;
        end %if strcmp(input,'e')==1
        
        kill = 0;
        if strcmp(input,'k')==1
            kill = 1;
            disp('**Trial Killed**')
            break;
        end
        
        if strcmp(input,' ') == 1
            [cancelled] = false;
            kill = 0;
            disp('**Next Trial**')
            break;
        end
        
    end %end while(1)(End of Trial Editor)
    
    % get the interval to the peaks
    
    %___________________________________________________________________________
    %END OF KINEMATIC ANALYSIS
    
    %calc grip app for 0-100% of movement time.
    %first figure out epochs
    tenth_mt1 = round((end_frame1 - rt_frame1)/10);
    
    for j1=1:11
        percentx1(j1)=x1(rt_frame1+((j1-1)*tenth_mt1));
        percenty1(j1)=y1(rt_frame1+((j1-1)*tenth_mt1));
        %percentz1(j1)=z1(rt_frame1+((j1-1)*tenth_mt1));
    end
    try % to write-out the matrix for the output-file
        k=1; %counter for output columns
        output{k} = trial_number;k=k+1;
        switch kill
            case 1
                output{k} = 'BAD';
                good_data = 0;
            otherwise
                output{k} = 'Good';
                good_data = 1;
        end
        k=k+1; %go to next column
        %ired number
        %output{k} = ired;	k=k+1;
        %reaction time
        output{k} = rt_frame1*(1000/frequency);k=k+1;
        %movement time
        output{k} = (end_frame1 - rt_frame1)*(1000/frequency);k=k+1;
        output{k} = true_angle_deg;k=k+1;
        output{k} = deviation_angle;k=k+1;
%             if (abs(targetvec(1)-8.6603)<0.0001) && ((targetvec(1)*peakVvec(2)-targetvec(2)*peakVvec(1))>0)
%             output{k} = angle4;k=k+1;
%             elseif (abs(targetvec(1)-8.6603)<0.0001) && ((targetvec(1)*peakVvec(2)-targetvec(2)*peakVvec(1))<0)
%             output{k} = angle4;k=k+1;
%             elseif (abs(targetvec(1)-0)<0.0001) && ((targetvec(1)*peakVvec(2)-targetvec(2)*peakVvec(1))>0)
%             output{k} = angle12;k=k+1;
%             elseif (abs(targetvec(1)-0)<0.0001) && ((targetvec(1)*peakVvec(2)-targetvec(2)*peakVvec(1))<0)
%             output{k} = angle12;k=k+1;
%             elseif (abs(targetvec(1)+8.6603)<0.0001)  && ((targetvec(1)*peakVvec(2)-targetvec(2)*peakVvec(1))>0)
%             output{k} = angle8;k=k+1;
%             else
%             output{k} = angle8;k=k+1;
%             end
        %peak acceleration s
        output{k} = pas1;k=k+1;
        output{k} = tpas1;k=k+1;
        %         output{k} = pasdisp1(1);k=k+1;
        %         output{k} = pasdisp2(1);k=k+1;
        %         output{k} = pasdisp1(2);k=k+1;
        %         output{k} = pasdisp2(2);k=k+1;
        %         output{k} = pasdisp1(3);k=k+1;
        %         output{k} = pasdisp2(3);k=k+1;
        %peak velocity s
        output{k} = pvs1;k=k+1;
        output{k} = tpvs1;k=k+1;
        output{k} = pvsdisp1(1);k=k+1;
        output{k} = pvsdisp1(2);k=k+1;

        %peak deceleration s
        %         output{k} = pds1;k=k+1;
        %         output{k} = tpds1;k=k+1;
        % 		  output{k} = pdsdisp1(1);k=k+1;
        %         output{k} = pdsdisp2(1);k=k+1;
        %         output{k} = pdsdisp1(2);k=k+1;
        %         output{k} = pdsdisp2(2);k=k+1;
        %         output{k} = pdsdisp1(3);k=k+1;
        %         output{k} = pdsdisp2(3);k=k+1;
        
        %position at x,y,z position @ movement start and end.
        output{k} = startposx1; k=k+1;
        output{k} = startposy1; k=k+1;
        output{k} = startposz1; k=k+1;
        output{k} = endposx1; k=k+1;
        output{k} = endposy1; k=k+1;
        output{k} = endposz1; k=k+1;
        
        %position at deciles
        for j1=1:11
            output{k} = percentx1(j1);k=k+1;
            output{k} = percenty1(j1);k=k+1;
            %output{k} = percentz1(j1);k=k+1;
        end
        close all;
    catch % problem writing out matrix
        
        output{k} = trial_number;k=k+1;
        switch kill
            case 1
                output{k} = 'BAD';
            otherwise
                output{k} = 'g';
        end
        k=k+1;
        output{k} = 9999;
        for j = 13:60
            output{j} = 999999999;
        end
    end % try        
    close all;    
    i= i+1;%increase trial counter
    A = 'A'; %set column for excell output
    xlscell = strcat(A,num2str(i)); % combine the letter 'A' with a number, q, to create 'AQ' as a cell to write data
    xlswrite(subject_code,output,1,xlscell);
    
%     if i = 1
%         (t = t - 2991-960);
   
%     if i>3
%         Slope4 =polyfit(1:20,angle4(11:30),1);
%         Slope8 =polyfit(1:20,angle8(11:30),1);
%         Slope12 =polyfit(1:20,angle12(11:30),1);
%         
%         Gradual4 = [mean(angle4(46:50))];
%         Gradual8 = [mean(angle8(46:50))];
%         Gradual12= [mean(angle12(46:50))];
%     else
%     end

end
%     xlswrite(subject_trajectory,allx,1,xlscell);
%     xlswrite(subject_trajectory,ally,2,xlscell);
    
    
disp('All Trials Analyzed')