Robert Hermosillo 

Reach analysis: bobby_analyze_8 was written to analyze motion-capture data using initally an Optotrack system, 
but later modified to use a Polhemius EM tracking system.

THis code is was designed to read the real position data, and to calculate a reach vector based on altered visual feedback.
Participants reach under rotated feedback conditions, and as a result learn to adapt to conditions.  The code generates an output file with 
reaction time, movement, time to peak velocity, reach angle, reach deviation, and other movement parameters.