Robert Hermosillo 4/27/2015

TMS_TOJ: 
contains 3 files: TMS_TOJ_main, TMS_TOJ_block, TMS_TOJ_trial

These matlab files are used to experiments with an optotrack motion capture system.  The code is designed to collected motion data 
about infrared sensors, and calulates movement trajectories based on a defined coordinate system.  At the same time, the code
sends pings to a NI board (National Instruments) to control the timing of piezo electric motors, and TTL puleses (triggers magnetic stimulation device). 