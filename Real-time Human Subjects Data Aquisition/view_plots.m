function [fig1 fig2 fig3 fig4 fig5 Uinput Comments] = view_plots(plot_azimuth,plot_elevation,pos1, pos2, pos3, pos4, pos5, file_number, rt_frame, end_frame, frequency, pas, ipas, pvs,ipvs, pds, ipds,t, x, y, z, dx, dy, ds, dds,M_s_dist,tmsd,M_s_dist_frame)
clicked = 0;
%view  data of last trial in plot form
	%fignam1 = strcat(file_number,': Position');
    fignam1 = strcat(file_number,': Position');
    fig1 = figure('Name',fignam1,'NumberTitle','off','Units','normalized','Position',pos1);
    hold on
    plot (t,x-x(1),'Color','k');
    plot(t,y-y(1), 'Color',[0.4,0,0.7]);
    plot(t,z-z(1),'Color',[0,0.1,0.85]);
    title('Hand Position');
	legend('X','Y','Z');
	grid on;
	v=axis;
	xlabel('Time(samples)');
	ylabel('Displacement(mm)');
	line([rt_frame,rt_frame],[v(3),v(4)],'Color','g');
	line([end_frame,end_frame],[v(3),v(4)],'Color','r'); 
    whitebg([1,1,1]);
    hold off
        
	fignam2 = strcat('trial ',file_number,': Velocity');
	fig2 = figure('Name',fignam2,'NumberTitle','off','Units','normalized','Position',pos2);
	hold on
    plot(t,dx,'Color','k');
    plot(t,dy,'Color',[0.4,0,0.7]);
    plot(t,ds,'Color',[0,0.1,0.85]);
	title('Hand Speed');
	legend('X-velocity','Y-velocity','S-velocity');
	xlabel('Time(samples)');
	ylabel('Velocity (mm/s)');
	grid on;
	v=axis;
	line([ipvs,ipvs],[v(3),v(4)],'Color','y');
	line([rt_frame,rt_frame],[v(3),v(4)],'Color','g');
	line([end_frame,end_frame],[v(3),v(4)],'Color','r');
    hold off
     
	fignam3 = strcat('trial ',file_number,': Acceleration');
	fig3 = figure('Name',fignam3,'NumberTitle','off','Units','normalized','Position',pos3);
	plot(t,dds,'Color','k');
	title('Hand Acceleration');
	legend('S-accel');
	xlabel('Time(samples)');
	ylabel('Acceleration (mm/s2)');
	grid on;
	v=axis;
	line([ipvs,ipvs],[v(3),v(4)],'Color','y');
	line([rt_frame,rt_frame],[v(3),v(4)],'Color','g');
	line([end_frame,end_frame],[v(3),v(4)],'Color','r');
              
   
    fignam4 = strcat('trial ',file_number,': 3 Dimensional View of Displacement');
    fig4 = figure('Name',fignam4,'NumberTitle','off','Units','normalized','Position',pos4);
    hold on
    view([plot_azimuth,plot_elevation]);
    plot3(x,y,z,'.','Color','k');
    plot3(x(ipvs),y(ipvs),z(ipvs),'o','Color','y');
    plot3(x(rt_frame),y(rt_frame),z(rt_frame),'o','Color','g');
    plot3(x(M_s_dist_frame+rt_frame),y(M_s_dist_frame+rt_frame),z(M_s_dist_frame+rt_frame),'o','Color','c');
    plot3(x(end_frame),y(end_frame),z(end_frame),'o','Color','r');
    grid on
    xlabel('x-axis (mm)');
    ylabel('y-axis (mm)');
    zlabel('z-axis (mm)');
    title('3-Dimensional view of Hand Displacement (mm)');
    %%%%axis([-50 50 -100 350 -10 120])
    
    hold off  
    disp('***** ViewPlots *****');
    fignam5 = strcat('Trial Summaries');
    set(0,'CurrentFigure',fig4);
    rt =(int2str(rt_frame*(1000/frequency)));
    mt =(int2str((end_frame - rt_frame)*(1000/frequency)));
    pa =(int2str(pas));
    tpa = (int2str((ipas-rt_frame)*(1000/frequency)));
    pv = (int2str(pvs));
    tpv = (int2str((ipvs-rt_frame)*(1000/frequency)));
    pd = (int2str(pds));
    tpd = (int2str((ipds-rt_frame)*(1000/frequency)));
    [fig5 Uinput Comments] = reach_trial_summary_table(rt,mt,pv,tpv,pa,tpa,pd,tpd,fignam5,pos5,M_s_dist,tmsd);    

    
    
return