% Run_Basic_Block.m
% This Program will run a block of trials using the new Load_Experiment_Parameters.m and Run_Basic_Trial.m
% Otherwise it runs very similarly to the previously used 'Run_Block' '.m' files.
function [trial_counter,block_counter, Cancelled] = TMS_TOJ_block(block_type,fid_main,part_dir,experiment_directory,participant,trial_counter,block_counter,T_glob_loc,num_ireds, sample_freq, collection_time,button_start)
Cancelled = false;    
% Check if the appropriate number of ireds are present for TOJ task 
    if strcmp(block_type,'TMS_TOJ') == 1
        if num_ireds ~= 2
            disp('Innappropriate Number of IREDS for a TOJ Block');
            disp('Incorrect Block file chosen.');
            fclose(fid_main);
            TransputerShutdownSystem();
            clear all; close all; Screen('CloseAll'); 
            disp('*** Exiting Program ***');
            return;
        end % if
    end %if
    % Hard Coded Variable Declarations:   
    [block_trial] = 0;
    [trial_start_time] = [];    
    [Go_Sound] = sin(1:300);
    [Sound_Hz] = 2500;
    [Lock_Ping] = 255;
    [End_GOOD_TRIAL_Ping] = 111;
    [End_KILLED_TRIAL_Ping] = 112;
    % Set defaults for REACHANALYZE FILTER STUFF
    [butt_cutoff] = 10;
    [butt_order] = 2;    
    % Set defaults for 3d plot 
    [plot_azimuth] = 125;
    [plot_elevation] = -13;
    % Set defaults for figure locations for analysis plots
    [fig1_pos] = [0.00292969 0.532552 0.305703 0.343698];
    [fig2_pos] = [0.00292969 0.0643 0.305703 0.343698];
    [fig3_pos] = [0.355,0.0643 0.305703 0.343698];
    [fig4_pos] = [0.355 0.532552 .62 0.343698];
    [fig5_pos] = [0.71 0.0643 0.2503 0.4030698];
    % start killed counter
    [killed_counter] = 0;      
    %Open a Window for the stimuli
    [w1, r] = Screen('OpenWindow',2,[0 0 0]);            
    %% SET UP PARALLEL PORT FOR LOCK PING ... ie only set up EEG Pins first so
    daqreset; % This re-sets the software (i.e., DLLs) for the NI box
    % that timing of tic and ping will be as close as possible
    dio = digitalio('nidaq','dev1'); % sets up parallel ports
    EEG = addline(dio,0:7,1,'out'); % identifies the output pins for motors
    Dio_device = addline(dio,0:7,0,'out'); % identifies output pins of Goggles and/or LED.  
    Goggles = addline(dio,3:4,2,'out');
    
    putvalue(Goggles,0);putvalue(Dio_device,0);putvalue(EEG,0);
    
    commandwindow;
    all_files_found = false;
    disp(block_type);
    while all_files_found == false
        %% Set_up Trials
        [bf_file] = uigetfile('*.bf*','Please choose a .param file:');
        % if they clicked cancel, exit the block gracefully
        if bf_file == 0
            [Cancelled] = true;
            disp('Program Exited by Clicking Cancel in file selection window');
            return
        end
        %% Read in the Experiment Trial Information
        [block_type_retrieved trials_remaining Condition_names Fixation1 Fixation2 Target1 Target2 Target3 ERROR MESSAGE] = Load_Experiment_Parameters(block_type,bf_file);
        if strcmp(block_type_retrieved,block_type) == 1
            % Check 'Images' Directory for Image files
        [all_files_found] = check_for_images2(Fixation1,Fixation2,Target1,Target2,Target3);        
        else
            disp('Innapropriate block type listed in retrieved file. Please choose again.');
        end
    end %while    
    % Cue the initiation of the  block
    [FIG C] = Block_initiation_figure(experiment_directory,participant,int2str(block_counter),bf_file,int2str(sum(trials_remaining)),int2str(num_ireds),int2str(collection_time));
    % if they clicked cancel, exit the block gracefully
    delete(FIG);
    if strcmp(C,'true') == 1
        Cancelled = true;
        return
    end    
    % Write Header info into main data file for experiment
    write_output_header(fid_main,num_ireds,participant,block_type);      
    % run the trials
    while sum(trials_remaining)>0
        % Reset the RandomSeed
        rand('seed',fix(100*sum(clock)));
        % Randomly pick a condition from the trials_remaining variable
        %current_trial = ceil(rand(1,1)*(length(trials_remaining)));
        current_trial = randi(length(trials_remaining));
        % If there are Trials remaining for that condition... Run a Trial... If NOT... Randomly Select again.
        if trials_remaining(current_trial) > 0                      
            good_data = 1;
            
            [ERROR MESSAGE Cancelled good_data output trial_output fig1_pos fig2_pos fig3_pos fig4_pos fig5_pos plot_azimuth plot_elevation Fix1_time Fix2_time Targ1_time Targ2_time Targ3_time Comments Where_did_you_feel_it] = TMS_TOJ_trial(num_ireds,T_glob_loc,block_type,trial_counter,part_dir,w1,Go_Sound,Sound_Hz,dio,Dio_device,trials_remaining,killed_counter,Condition_names(current_trial),Fixation1(current_trial),Fixation2(current_trial),Target1(current_trial),Target2(current_trial),Target3(current_trial),fig1_pos,fig2_pos,fig3_pos,fig4_pos,fig5_pos,plot_azimuth,plot_elevation,butt_cutoff,butt_order);            
            % Re-APPLY the NEGATIVE values to the timings when writing out the trial info to output file
            % REMEMBER that negative VALUES REPRESENT TARGET IMAGES that occurred PRIOR TO GO SIGNAL ... TOTAL time of PRESENTATION IS STILL the absolute value
            if Target1(current_trial).minduration < 0
                timeTarg1 = Targ1_time*(-1);
            else
                timeTarg1 = Targ1_time;
            end
            
            if Target2(current_trial).minduration < 0
                timeTarg2 = Targ2_time*(-1);
            else
                timeTarg2 = Targ2_time;
            end
            
            if Target3(current_trial).minduration < 0
                timeTarg3 = Targ3_time*(-1);
            else
                timeTarg3 = Targ3_time;
            end            
            % write out the initial trial information
            fprintf(fid_main,'\n%d\t',block_counter);
            fprintf(fid_main,'%s\t',block_type);
            fprintf(fid_main,'%d\t',trial_counter);
            fprintf(fid_main,'%s\t',Condition_names{current_trial});            
%             fprintf(fid_main,'%s\t',Fixation1(current_trial).image);
%             fprintf(fid_main,'%d\t',Fix1_time);
%             fprintf(fid_main,'%d\t',Fixation1(current_trial).diostate);
%             fprintf(fid_main,'%d\t',Fixation1(current_trial).eeg);            
%             fprintf(fid_main,'%s\t',Fixation2(current_trial).image);
%             fprintf(fid_main,'%d\t',Fix2_time);
%             fprintf(fid_main,'%d\t',Fixation2(current_trial).diostate);
%             fprintf(fid_main,'%d\t',Fixation2(current_trial).eeg);            
%             fprintf(fid_main,'%s\t',Target1(current_trial).image);
%             fprintf(fid_main,'%d\t',timeTarg1);
%             fprintf(fid_main,'%d\t',Target1(current_trial).diostate);
%             fprintf(fid_main,'%d\t',Target1(current_trial).eeg);            
%             fprintf(fid_main,'%s\t',Target2(current_trial).image);
%             fprintf(fid_main,'%d\t',timeTarg2);
%             fprintf(fid_main,'%d\t',Target2(current_trial).diostate);
             fprintf(fid_main,'%d\t',Target2(current_trial).eeg);            
%             fprintf(fid_main,'%s\t',Target3(current_trial).image);
%             fprintf(fid_main,'%d\t',timeTarg3);
%             fprintf(fid_main,'%d\t',Target3(current_trial).diostate);
             fprintf(fid_main,'%d\t',Target3(current_trial).eeg);
            
            % write the rest of the trial data to the main output file
            [len2 wid2] = size(output);
            
            % write out file number and 'Good-or-bad Trial'
            fprintf(fid_main,'%s\t',output{1});
            fprintf(fid_main,'%s\t',output{2});
            good_data;
            %write out rest of trial data 
            for j = 3:wid2
                fprintf(fid_main,'%d\t',output{j});                
            end
            fprintf(fid_main,'%s\t',Comments);
            fprintf(fid_main,'%u\t',Where_did_you_feel_it);
            if(good_data) == 1
               trials_remaining(current_trial) = trials_remaining(current_trial) - 1;
               % if data was good... ping EEG happy ping
               putvalue(EEG,End_GOOD_TRIAL_Ping);
               disp(strcat(['Trials remaining = ',num2str(sum(trials_remaining))]));
           else
               killed_counter = killed_counter + 1;
               disp('Last Trial was Killed.');
               disp(strcat(['Trials remaining = ',num2str(sum(trials_remaining))]));
               % if data was killed... ping EEG a sad ping
               putvalue(EEG,End_KILLED_TRIAL_Ping);
           end % if good data        
           commandwindow;
        
           % if they tried to exit... do so
           if Cancelled == true
              return;
           end                   
       
        trial_counter = trial_counter + 1;
        close all
        end % if trials_remaining  
    end %while trials_remaining
    Screen('Closeall');
return; % end function
