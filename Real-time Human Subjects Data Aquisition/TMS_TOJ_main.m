%Clear/Close the workspace and any open windows
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%clear all; close all; Screen('CloseAll');
 
% Generic Variable Declarations
button_start = 0;

[num_ireds] = 2;
[sample_freq] = 500;
[collection_time] = 2.5;  

% set up experiment
[experiment_directory] = uigetdir('C:\Program Files\MATLAB\R2009a\work\OPTO_Work\ndlib\Experiments Folder\TMS_TOJ_main','Please Select the Correct Experiment Folder:');
%[experiment_directory] =  'C:\Program Files\MATLAB\R2009a\work\OPTO_Work\ndlib\Experiments Folder\M_Heath_EEG';
cd (experiment_directory);
% Get the participant number and create a new directory for them
[participant part_dir] = get_participant(experiment_directory);
%Open an output file for the participant's data
part_file = strcat(participant,' .dat');
[fid_main message_main] = fopen(part_file,'w');
% Get demographic info for the participant
[ERROR MESSAGE demo_info] = Get_Demographic_Info(fid_main,participant);
MESSAGE;
if ERROR == 1
    fclose(fid_main);    
    clear all; close all; Screen('CloseAll'); 
    disp('*** Exiting Program ***');
    return
end;

%Initialize Optotrak
optotrak_init();
%collect reference frame info


T_glob_loc = optotrak_transform(part_dir);

% MOVE THIS Stuff INTO target_locations_block.m ***************************************************
%Map out potential Target Locations
% dio = digitalio('nidaq','dev1'); % sets up parallel ports
% LED = addline(dio,0,1,'out');
% putvalue(LED,1);
% %%*************************************************************************************************
% target_locations_block (fid_main,part_dir,experiment_directory,participant,T_glob_loc)
cd (experiment_directory);
% Set the trial_counter
trial_counter = 1;
%%%%%%%%%%%%%%%%%%%%%%%%%% BLOCK COUNTER SET %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%If you are running a multiple day experiment adjust this value accordingly.  %%%%
block_counter = 1;


%% Create an array to call and randomize the block order
number_of_blocks = 2;
block_order = zeros(1,number_of_blocks);
for i = 1:number_of_blocks
    block_order(i) = i;
end
rand('seed',fix(100*sum(clock)));
block_order = Shuffle(block_order);

% Create a loop that runs all of the blocks
for current_block = 1:number_of_blocks
    if block_order(current_block) == 1 %|| block_order(current_block) == 2        
        if block_order(current_block) == 1
            block_type = 'TMS_TOJ';
            choice = menu(strcat(['Block type: ',block_type]),'OK');
        else
           fclose(fid_main);
           TransputerShutdownSystem();
           clear all; close all; Screen('CloseAll');
           disp('No Block type identified or selected.');
           disp('********* Exiting Program ***********');
        end % if
        [num_ireds, sample_freq, collection_time] = set_up_optotrak(num_ireds, sample_freq, collection_time);
        [trial_counter,block_counter,Cancelled] =TMS_TOJ_block(block_type,fid_main,part_dir,experiment_directory,participant,trial_counter, block_counter,T_glob_loc,num_ireds, sample_freq, collection_time,button_start);
        if Cancelled == true
        fclose(fid_main);
            TransputerShutdownSystem();
            clear all; close all; Screen('CloseAll'); 
            disp('*** Exiting Program ***');
            return 
        end
    end % if
    block_counter = block_counter+1;
end % for current_block

ShowCursor();
%ListenChar(1);
%close the participants' file
fclose(fid_main);
%Shutdown System
TransputerShutdownSystem();
% Clear/Close any open variables, objects, and windows
clear all; close all; Screen('CloseAll');