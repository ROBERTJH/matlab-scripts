% Run_Basic_Trial.m
% GENERAL PEOGRAM INFORMATION
% Version 1.0	Modified: May 23rd 2010	By: John de Grosbois
% Sensorimotor Neuroscience Lab: UBC-Okanagan
% [varargout] = Run_Basic_Trial(varargin)
%
% BRIEF PROGRAM DESCRIPTION
% This program has been designed to run a simple trial sequence using
% trial information gathered using the new 'Load_Experiment_Parameters.m'
% The sequence displays the images  Fixation1 Fixation2 and Targets 1-3
% The 'go-signal' occurrs at the point after Fixation2 when the min-durations become positive ... OR after all 3 target images have been displayed
% NOTE the inputs are Struct arrays that have the fields output by 'Load_Experiment_Parameters.m'
function [ERROR MESSAGE Cancelled good_data output trial_output fig1_pos fig2_pos fig3_pos fig4_pos fig5_pos plot_azimuth plot_elevation Fix1_time Fix2_time Targ1_time Targ2_time Targ3_time Comments Where_did_you_feel_it] = TMS_TOJ_trial(num_ireds,T_glob_loc,block_type,trial_counter,part_dir,winPointer,Go_Sound,Sound_Hz,dio,Dio_device,trials_remaining,killed_counter,Condition_name,Fixation1,Fixation2,Target1,Target2,Target3,fig1_pos,fig2_pos,fig3_pos,fig4_pos,fig5_pos,plot_azimuth,plot_elevation,butt_cutoff,butt_order)                                                                                                                                                                                                                                   
            
% if downstate == 1
%     upstate = 0;
% else
%     upstate = 1;
% end
motor_time = .1;
buzzer_activation = 128;
buzzer_wait_time = 0.08;
buzzer_deactivation = 0;
TMS_trigger_on = 4;
TMS_trigger_off = 0;
button_start = false; % HACK because button is not used in this experiment.
%% Declaring variables for the the buffer
    trial_output = {};
    [puRealtimeData]=0;
    [puSpoolComplete]=0;
    [puSpoolStatus]=0;
    [pulFramesBuffered]=0;   
	% PreLoad / MakeTextures of all The Images for the Chosen Trial
	[F1] = imread(Fixation1.image);
	[F2] = imread(Fixation2.image);
	[T1] = imread(Target1.image);
	[T2] = imread(Target2.image);
	[T3] = imread(Target3.image);
	[Fix1] = Screen('MakeTexture',winPointer,F1);
	[Fix2] = Screen('MakeTexture',winPointer,F2);
	[Targ1] = Screen('MakeTexture',winPointer,T1);
	[Targ2] = Screen('MakeTexture',winPointer,T2);
    [Targ3] = Screen('MakeTexture',winPointer,T3);
    
    F1_binvec = horzcat(dec2binvec(Fixation1.eeg,8),dec2binvec(Fixation1.diostate,3),dec2binvec(Fixation1.goggles,2));
    F2_binvec = horzcat(dec2binvec(Fixation2.eeg,8),dec2binvec(Fixation2.diostate,3),dec2binvec(Fixation2.goggles,2));
    Targ1_binvec = horzcat(dec2binvec(Target1.eeg,8),dec2binvec(Target1.diostate,3),dec2binvec(Target1.goggles,2));
    Targ2_binvec = horzcat(dec2binvec(Target2.eeg,8),dec2binvec(Target2.diostate,3),dec2binvec(Target2.goggles,2));
    Targ3_binvec = horzcat(dec2binvec(Target3.eeg,8),dec2binvec(Target3.diostate,3),dec2binvec(Target3.goggles,2));
       
    % Calculate the 'Delay-Values' for Image durations for the current trial
    [Fix1_time] = ((abs(Fixation1.minduration))+(round(rand(1,1)*Fixation1.maxrange)))/1000;
    [Fix2_time] = ((abs(Fixation2.minduration))+(round(rand(1,1)*Fixation2.maxrange)))/1000;
    [Targ1_time] = ((abs(Target1.minduration))+(round(rand(1,1)*Target1.maxrange)))/1000; 
    [Targ2_time] = ((abs(Target2.minduration))+(round(rand(1,1)*Target2.maxrange)))/1000;    
    [Targ3_time] = ((abs(Target3.minduration)+(round(rand(1,1)*Target3.maxrange))))/1000;    
%%  
    veclength = length(F1_binvec);
    % Prepare the input file
    file_number= [];
    [file_number] = get_file_number(trial_counter);
    %Set-up the the input_file for the current trial
    input_file = [];
    input_file = strcat(part_dir,'\',file_number);
    DataBufferInitializeFile(0,input_file);
    
	% Display Fixation1 IMAGE... set the digitalI/O values associated with Fixation1
	Screen('DrawTexture',winPointer,Fix1);
	putvalue(dio.Line(1:veclength),F1_binvec);
	Screen('Flip',winPointer);
    %choice = menu('Click OK to continue','OK');
    % Wait for the Min-Duration + Randomly Assigned Range value of Fixation1
	WaitSecs(Fix1_time);
	% Cue the Experimentor to start the trial OR Wait for a button-change
	if button_start == true
		disp(Condition_name);
        disp(strcat(['File: ',file_number]));
        disp(strcat(['Trial Number: ',(int2str((trial_counter)))]));
        disp(strcat(['Number of Killed Trials: ',(int2str(killed_counter))]));        
        % Check the button iteratively for a lift
		Ready_Button(dio,button.Index,upstate);       
	else
		% Cue to experimentor to start the trial        
        % Determine values for figure
        [title] =(strcat('File: ',file_number));
        [trials_complete] = (int2str(trial_counter));
        [killed_trials] = (int2str(killed_counter));
        [name_of_trial] = Condition_name;
        lstbox_array = {Fixation1.image; Fixation2.image; Target1.image; Target2.image; Target3.image};
        [ready_fig Inquiry] = trial_initiation(title,trials_complete,killed_trials,name_of_trial,lstbox_array);
        delete(ready_fig);
        if Inquiry == 0
            Cancelled = true;
            ERROR = 0;
            MESSAGE = 'Program Exited.';
            return
        else
            Cancelled = false;
        end
	end % if
    Priority(1);
 
	% Display Fixation2 image and set the digitalI/O values for that image
	Screen('DrawTexture',winPointer,Fix2);
	Screen('Flip',winPointer);
    putvalue(dio.Line(1:veclength),F2_binvec);
	
	% Wait for the Min-Duration + Randomly Assigned Range value of Fixation2
	WaitSecs(Fix2_time);
	
    % Make sure the Optotrak Markers are active
    
	% Now depending on where the 'go-signal' will be, run out the rest of the trial sequence
	% NOTE: the 'Go-signal' is played after the last Target whose .minduration is negative
	% If none are negative the Go-Signal is Played First... If all are negative Go-signal is played last
	if (Target1.minduration < 0 && Target2.minduration < 0 && Target3.minduration < 0) % If all targets are DISPLAYED PRIOR to the go-signal
		% Display Target1 image and set the digitalI/O values for that image
		Screen('DrawTexture',winPointer,Targ1);
		putvalue(dio.Line(1:veclength),Targ1_binvec);
		Screen('Flip',winPointer);
		% Wait for the Min-Duration + Randomly Assigned Range value of Target1
		WaitSecs(Targ1_time);
		
		% Display Target2 image and set the digitalI/O values for that image
		Screen('DrawTexture',winPointer,Targ2);
		putvalue(dio.Line(1:veclength),Targ2_binvec);
		Screen('Flip',winPointer);
		% Wait for the Min-Duration + Randomly Assigned Range value of Target2
		WaitSecs(Targ2_time);

		% Display Target3 image and set the digitalI/O values for that image
		Screen('DrawTexture',winPointer,Targ3);
		putvalue(dio.Line(1:veclength),Targ3_binvec);
		Screen('Flip',winPointer);
		% Wait for the Min-Duration + Randomly Assigned Range value of Target3
		WaitSecs(Targ3_time);

		% Start the Data_Buffer and Play the Go-Signal
		Optotrakactivatemarkers();
        DataBufferStart();
		Snd('Play',Go_Sound,Sound_Hz);
		spool_data(puRealtimeData,puSpoolComplete,puSpoolStatus,pulFramesBuffered);
	elseif (Target1.minduration < 0 && Target2.minduration < 0 && Target3.minduration > 0) % If targets 1 and 2 are DISPLAYED PRIOR to the go-signal
		% Display Target1 image and set the digitalI/O values for that image
		Screen('DrawTexture',winPointer,Targ1);
		putvalue(dio.Line(1:veclength),Targ1_binvec);
		Screen('Flip',winPointer);
		% Wait for the Min-Duration + Randomly Assigned Range value of Target1
		WaitSecs(Targ1_time);
		
		% Display Target2 image and set the digitalI/O values for that image
		Screen('DrawTexture',winPointer,Targ2);
		putvalue(dio.Line(1:veclength),Targ2_binvec);
		Screen('Flip',winPointer);
		% Wait for the Min-Duration + Randomly Assigned Range value of Target2
		WaitSecs(Targ2_time);
		
		% Display Target3 image and set the digitalI/O values for that image
		Screen('DrawTexture',winPointer,Targ3);
		putvalue(dio.Line(1:veclength),Targ3_binvec);
        % Start the Data_Buffer and Play the Go-Signal
		Optotrakactivatemarkers();
        DataBufferStart();
		Snd('Play',Go_Sound,Sound_Hz);
        Screen('Flip',winPointer);
		% Wait for the Min-Duration + Randomly Assigned Range value of Target2
		WaitSecs(Targ3_time);
        spool_data(puRealtimeData,puSpoolComplete,puSpoolStatus,pulFramesBuffered);
	elseif (Target1.minduration < 0 && Target2.minduration > 0 && Target3.minduration > 0) % If target1 is DISPLAYED PRIOR to the go-signal
		% Display Target1 image and set the digitalI/O values for that image
		Screen('DrawTexture',winPointer,Targ1);
		putvalue(dio.Line(1:veclength),Targ1_binvec);
		Screen('Flip',winPointer);
		% Wait for the Min-Duration + Randomly Assigned Range value of Target1
		WaitSecs(Targ1_time);
		
		% Display Target2 image and set the digitalI/O values for that image
		Screen('DrawTexture',winPointer,Targ2);
		putvalue(dio.Line(1:veclength),Targ2_binvec);
        % Start the Data_Buffer and Play the Go-Signal
		Optotrakactivatemarkers();
        DataBufferStart();
		Snd('Play',Go_Sound,Sound_Hz);
        Screen('Flip',winPointer);
		% Wait for the Min-Duration + Randomly Assigned Range value of Target2
		WaitSecs(Targ2_time);

		% Display Target3 image and set the digitalI/O values for that image
		Screen('DrawTexture',winPointer,Targ3);
		putvalue(dio.Line(1:veclength),Targ3_binvec);
		Screen('Flip',winPointer);
		% Wait for the Min-Duration + Randomly Assigned Range value of Target2
		WaitSecs(Targ3_time);
        spool_data(puRealtimeData,puSpoolComplete,puSpoolStatus,pulFramesBuffered);
	elseif (Target1.minduration > 0 && Target2.minduration > 0 && Target3.minduration > 0) % If all targets are displayed AFTER to the go-signal
		% Display Target1 image and set the digitalI/O values for that image
		Screen('DrawTexture',winPointer,Targ1);
		%putvalue(dio.Line(1:veclength),Targ1_binvec);
        
        %START OF TRIAL PROPER
        trial_name = char(Condition_name);
        Screen('Flip',winPointer);
        %Snd('Play',Go_Sound,Sound_Hz);
        switch trial_name
            
            case '250_Right_Hand_First'
                % Start the Data_Buffer and Play the Go-Signal
                OptotrakActivateMarkers();
                WaitSecs(.05); %give markers time to turn on
                DataBufferStart();
                %Snd('Play',Go_Sound,Sound_Hz);
                putvalue(Dio_device,buzzer_activation);WaitSecs(buzzer_wait_time);putvalue(Dio_device,buzzer_deactivation);%buzz
                %WaitSecs(1);
                %putvalue(Dio_device,TMS_trigger_on);putvalue(Dio_device,TMS_trigger_off);
                WaitSecs(.250); %250ms delay
                for i=1:5;
                    putvalue(Dio_device,1);%turn on right vibrator
                    WaitSecs(0.002);%0.1=10Hz; 0.01=100Hz
                    putvalue(Dio_device,0);%turn off right vibrator
                    i = i+1;
                end
                WaitSecs(.100); %ISI
                for i=1:5;
                    putvalue(Dio_device,2);%turn on left vibrator
                    WaitSecs(0.002);
                    putvalue(Dio_device,0);%turn off left vibrator
                    i = i+1;
                end
                
              case '250_Left_Hand_First'
                % Start the Data_Buffer and Play the Go-Signal
                OptotrakActivateMarkers();
                WaitSecs(.05);
                DataBufferStart();
                %Snd('Play',Go_Sound,Sound_Hz);
                putvalue(Dio_device,buzzer_activation);WaitSecs(buzzer_wait_time);putvalue(Dio_device,buzzer_deactivation);
                %WaitSecs(1);
               % putvalue(Dio_device,TMS_trigger_on);putvalue(Dio_device,TMS_trigger_off);
                WaitSecs(.250);
                for i=1:5;
                    putvalue(Dio_device,2);
                    WaitSecs(0.002);
                    putvalue(Dio_device,0);
                    i = i+1;
                end
                WaitSecs(.100);
                for i=1:5;
                    putvalue(Dio_device,1);
                    WaitSecs(0.002);
                    putvalue(Dio_device,0);
                    i = i+1;
                end

             otherwise
                disp('Error! Please step through the above switch-case code.');
                disp('No cases match with those passed in!!!');
                disp('Please check your bf file to see if the trial names match the cases exactly.');
                disp('I.e., make certain the spelling exactly match!');                         
        end %end switch
        
        % Turn off the Motor
        putvalue(Dio_device,0);
        % Wait for the Min-Duration + Randomly Assigned Range value of Target1
		WaitSecs(Targ1_time);
		
		% Display Target2 image and set the digitalI/O values for that image
		Screen('DrawTexture',winPointer,Targ2);
		%putvalue(dio.Line(1:veclength),Targ2_binvec);
		Screen('Flip',winPointer);
        putvalue(dio.Line(1:veclength),Targ2_binvec);
        WaitSecs(motor_time);
		% Wait for the Min-Duration + Randomly Assigned Range value of Target2
		WaitSecs(Targ2_time);

		% Display Target3 image and set the digitalI/O values for that image
		Screen('DrawTexture',winPointer,Targ3);
		putvalue(dio.Line(1:veclength),Targ3_binvec);
		Screen('Flip',winPointer);
		% Wait for the Min-Duration + Randomly Assigned Range value of Target2
		WaitSecs(Targ3_time);
        % Spool the data and Clear the Sound and Display Buffers
        spool_data(puRealtimeData,puSpoolComplete,puSpoolStatus,pulFramesBuffered);
    end % if

	Snd('Quiet');
    Screen('Close',Fix1);
    Screen('Close',Fix2);
    Screen('Close',Targ1);
    Screen('Close',Targ2);
    Screen('Close',Targ3);
    
    % Deactivate the Markers
    OptotrakDeActivateMarkers();    
    %% Ask Participants about which hand was stimulated first (if any).
            Where_did_you_feel_it = menu('Where did you feel it?','Left hand','Right hand');
            
	%% Read-in the Optotrak Data and run Reach_Analyze.m on the data
	%Read back the data from the file c-file
    data = [];
    [fid_1, message] = fopen(deblank(input_file),'r+','l'); % little endian byte ordering
    % Read the header portion of the file
    [filetype,items,subitems,numframes,frequency,user_comments,sys_comments,descrip_file,cutoff,coll_time,coll_date,frame_start,extended_header,character_subitems,integer_subitems,double_subitems,item_size,padding] = read_nd_c_file_header(fid_1);
    % Read the data portion of the file
    data = read_nd_c_file_data(fid_1, items, subitems, numframes);
    fclose(fid_1);
    good_data = 1;
    
    try        
        % depending on the type of data / block type... run a different analysis routine
            %if strcmp(block_type,'No_Vision')==1 || strcmp(block_type,'Vision')==1
         [num_ireds,output,good_data]=bobby_analyse_1(T_glob_loc,num_ireds,file_number,data, items, numframes, frequency, butt_cutoff, butt_order,plot_azimuth, plot_elevation, fig1_pos, fig2_pos, fig3_pos, fig4_pos, fig5_pos);
         trial_output{1} = 'GOOD';
         trial_output{2} = 'GREAT';
         ERROR = 'None';
         MESSAGE = 'Good';
%          Cancelled = false;
%          good_data = true;
         Comments = 'All good';
    catch Me
        Me;
        trial_output = {};
        trial_output{1} = 'BAD';
        good_data = 0;
        MESSAGE = 'Analysis Routine Crashed and Trial was Dumped'; 
        Comments = 'Crashed Analysis';
        Cancelled = false;
        return % function
    end % try	
    Priority(0);
return %function

