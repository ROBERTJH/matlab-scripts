%December 5, 2015
%R. Hermosillo
%Sensorimotor Speech Phsyiology
%Department of Speech and Hearing Sciences
%University of Washington
%------------------

%% Difference spectrum for EEG Power spectra data:
%T.test on arrays function
% Provided that the datat are arranged with all the frequencies 0-50, and collected at 1k Hz for 1 sec.
%-----------------------------------------------------------------------------------------------------
clear all;close all; %Start Fresh!
disp('Loading excel data');
x = xlsread('EEG_TMS.xlsx', 'RT_P4_stat_arr', 'BA1:CX9000'); %real
y = xlsread('EEG_TMS.xlsx', 'RT_P4_stat_arr', 'C1:AZ9000'); %sham
real = zeros(1000,50,8);
sham = zeros(1000,50,8);
numbsub = 9;

SNL01_st = [];
SNL01_st = x(1:1000,1:50);
SNL02_st = [];
SNL02_st = x(1001:2000,1:50);
SNL03_st = [];
SNL03_st = x(2001:3000,1:50);
SNL04_st = [];
SNL04_st = x(3001:4000,1:50);
SNL05_st = [];
SNL05_st = x(4001:5000,1:50);
SNL06_st = [];
SNL06_st = x(5001:6000,1:50);
SNL07_st = [];
SNL07_st = x(6001:7000,1:50);
SNL08_st = [];
SNL08_st = x(7001:8000,1:50);
SNL09_st = [];
SNL09_st = x(8001:9000,1:50);

SNL01_rt = [];
SNL01_rt = y(1:1000,1:50);
SNL02_rt = [];
SNL02_rt = y(1001:2000,1:50);
SNL03_rt = [];
SNL03_rt = y(2001:3000,1:50);
SNL04_rt = [];
SNL04_rt = y(3001:4000,1:50);
SNL05_rt = [];
SNL05_rt = y(4001:5000,1:50);
SNL06_rt = [];
SNL06_rt = y(5001:6000,1:50);
SNL07_rt = [];
SNL07_rt = y(6001:7000,1:50);
SNL08_rt = [];
SNL08_rt = y(7001:8000,1:50);
SNL09_rt = [];
SNL09_rt = y(8001:9000,1:50);

real(:,:,1) = SNL01_rt; % put the SNL 50 x 1000 martix into the 1st level of the 3D cube.
real(:,:,2) = SNL02_rt;
real(:,:,3) = SNL03_rt;
real(:,:,4) = SNL04_rt;
real(:,:,5) = SNL05_rt;
real(:,:,6) = SNL06_rt;
real(:,:,7) = SNL07_rt;
real(:,:,8) = SNL08_rt;
real(:,:,9) = SNL09_rt;

sham(:,:,1) = SNL01_st;
sham(:,:,2) = SNL02_st;
sham(:,:,3) = SNL03_st;
sham(:,:,4) = SNL04_st;
sham(:,:,5) = SNL05_st;
sham(:,:,6) = SNL06_st;
sham(:,:,7) = SNL07_st;
sham(:,:,8) = SNL08_st;
sham(:,:,9) = SNL09_st;

disp('Calculating T values');
%[q,r,s] = differnce matrix
Tstat_matrix = zeros(1000,50);
pvalue_matrix = zeros(1000,50);
q = 1; %frequency counter
t = 1; %time counter
% for t =1:1000; %time counter
%     for q =1:50 %frequency counter
%     [H,P,CI, tstat] = ttest(real(t,q,1:9),sham(t,q,1:9),'tail','left'); %@ time 1, at frequency q, test subjects 1-9 against subjects 1 - 9
%     Tstat_matrix(t,q) = tstat.tstat(1);  
%     pvalue_matrix(t,q) = P; 
%     end
% end

%try
for t =1:1000; %time counter
    for q =1:50 %frequency counter
    [H,P,CI, tstat] = ttest(real(t,q,1:9),sham(t,q,1:9),'tail','left'); %@ time 1, at frequency q, test subjects 1-9 against subjects 1 - 9
    Tstat_matrix(t,q) = tstat.tstat(1);  
    if P>0.05
        P=NaN;
    else
    end  
    pvalue_matrix(t,q) = P;     
    end
end


imagelims = [0,0.05]; %set image limits
fig1 = surf(Tstat_matrix); hold off;
fig2 = imagesc(pvalue_matrix,imagelims);
colorbar;
hold on;