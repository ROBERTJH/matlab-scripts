# README #
This repository contains 3 files.
The first file, bobby_analyze_8.m, is used to Analyze 3-D motion capture data and calculate movement kinematic parameters.

The second, called array_tests.m, is an analysis of Electroencephalography power spectrum data.

The last set (TMS_TOJ_main.m, TMS_TOJ_block.m, and TMS_TOJ_trial.m) runs a full experimental protocol with motion capture while controlling external devices. (require a Northern Digital Inc (NDI) Optotrack and a National instruments (NI) output board. "TMS_TOJ_main.m" calls "TMS_TOJ_block.m" which then calls "TMS_TOJ_trial.m".

### What is this repository for? ###

* Quick summary
* Version 1.0

### How do I get set up? ###

 To run this code, download the paired files associated with them, (i.e. .xls or .mat data files), then open the files in Matlab 2013b or later.


### Who do I talk to? ###

If you have questions about this code, contact Robert: bhoebrbmy@gmail.com